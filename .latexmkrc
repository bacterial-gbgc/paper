$pdfclean_ext = 'bbl bcf tdo run.xml';
$pdf_mode = "1";
$pdflatex = "pdflatex --interaction='batchmode' -shell-escape";
$pdf_previewer = "open -a Skim.app";
$preview_continuous_mode = "0";