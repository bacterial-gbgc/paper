#' ---
#' title: "Distance Threshold"
#' date: "`r Sys.Date()`"
#' author: "Samuel Barreto"
#' output: tint::tintPdf
#' latexfonts:
#' - package: libertinus
#'   options:
#'     - osf
#'     - p
#' - package: gillius2
#' - package: zi4
#'   options:
#'     - varqu
#'     - varl
#' - package: libertinust1math
#' - package: siunitx
#' ---

#- r knitr_init, echo=FALSE, cache=FALSE
knitr::opts_chunk$set(
  echo=FALSE,
  prompt=FALSE,
  comment=NA,
  message=FALSE,
  cache=TRUE,
  warning=FALSE,
  global.par=TRUE,
  tidy=FALSE,
  fig.width=7,
  fig.height=5,
  dev="pdf",
  fig.showtext=TRUE,
  fig.path="figures/distthres/",
  cache.path=".cache/distthres/")

#' # Influence of between converted marker distance

source(".datas.f.R")
source("~/these/projets/papers/2018-09-17-experimental-evidence/analysis/utils.R")
library(data.table)
library(transformr)
library(samuer)
library(ggplot2)
library(Annotation.Abaylyi.GenBank.1)
library(BSgenome.Abaylyi.GenBank.1)
library(patchwork)

#' download vcf if need be.
if (!file.exists("../data/cohort.vcf"))
  download.file(
    url = "https://ndownloader.figshare.com/files/17783654",
    destfile = "../data/cohort.vcf")

wgt <- local({
  data("Annotation.abaylyi.genbank.1")
  gbk <- Annotation.abaylyi.genbank.1
  ## genome sequence in fasta format
  abgenome <- BSgenome.Abaylyi.GenBank.1
  ## uniform genome seqinfo
  seqlevels(abgenome) <- unname(genome(gbk))
  abgenome@seqinfo@genome <- names(genome(gbk))
  # read vcf file
  vcf <- readVcf("../data/cohort.vcf")
  ## ugly code to say remove high frequency sites.
  vcf <- vcf[-which(apply(
    geno(vcf)$GT, 1, function(x) {
      sum(!(x %in% c(".", "0/0")))
    }) / dim(vcf)[2] > 0.9), ]
  ## uniform seqlevels
  seqlevels(vcf) <- seqlevels(abgenome)
  seqinfo(vcf) <- seqinfo(abgenome)
  vcf <- .exclude(vcf, which(info(vcf)$MQ < 60))
  vcf <- .exclude(vcf, which(info(vcf)$QD < 20))
  ## find converted sites
  wgt <- WholeGenomeTransformation(vcf, "ab93a2_donor", "ab93a2_t50")
  ## ugly code to say remove high frequency converted sites
  #  1. represent each polymorphich position as a matrix
  #  2. sum for each position the number of time it is converted.
  #  3. remove all sites that are above 0.8 time converted
  vcf <- vcf[
    -which((apply(
      do.call("rbind", lapply(conversions(wgt), as.vector)), 2,
      sum) / 39) > 0.8),
    ]
  ## redo the conversion analysis on cleaned up VCF
  wgt <- WholeGenomeTransformation(vcf, "ab93a2_donor", "ab93a2_t50")
  wgt})

#' To check that the choice of distance threshold that sets two
#' conversion tracts apparts had no influence on the gBGC estimates,
#' I made this distance threshold range between extremely low and high
#' values (100 bp to 50 kbp).
#'
#' The distance threshold I chose are:

#- fig006, results="asis"

toLatex.double <- function(object, ...)
  cat(toString(paste0("\\num{", object , "}")))

(.dists_thres <- c(2e2, 5e2, 1e3, 2e3, 5e3)) %>%
  toLatex()

#' Then I compute resulting conversion tracts for all distance
#' threshold. The figure \ref{fig:gnm} represents the variety of
#' tracts obtained according to different distance thresholds.
#'
#' We can see that using a really large distance threshold gives some
#' unrealistic ranges of unconverted markers, while low values give
#' reasonably similar results (figure \ref{fig:pile}).

pmap <- parallel::mcMap
wgt_on <- function(.wgt) function(d) WGTConversionTracts(.wgt, d)
wgt_on_abay <- wgt_on(wgt)
wgts <- pmap(wgt_on_abay, .dists_thres)
names(wgts) <- as.character(.dists_thres)

#- fig000, eval=FALSE
# for interactive use only
while (TRUE) {
  sow2(wgts, names(wgts), function(x, n) {
    .c <- viridis(n = 90)[log10(as.numeric(n)) * 10]
    .p <- x %>%
      tracts() %>%
      ranges() %>%
      as.data.frame() %>%
      ggplot(aes(x = start, xend = end, y = group_name, yend = group_name)) +
      geom_segment(size = 3, color = .c) +
      labs(title = n)
    print(.p)
    Sys.sleep(1)
  })
}

#- fig001, fig.fullwidth=TRUE, fig.cap="\\label{fig:gnm}", fig.width=10

pile(map2(wgts, names(wgts), function(x, n) {
  .d <- as.data.frame(ranges(tracts(x)))
  .d$dist <- as.numeric(n)
  .d
})) %>%
  dplyr::arrange(desc(dist)) %>%
  dplyr::mutate(fdist = factor(dist)) %>%
  ggplot(aes(x = start, xend = end, y = group_name, yend = group_name)) +
  geom_segment(aes(color = fdist, alpha = dist), size = 3) +
  scale_color_viridis_d(end = 0.9, begin = 0) +
  scale_alpha_continuous(trans = "log10", range = c(1, 0.6)) +
  guides(alpha = "none") +
  theme_thesis() +
  theme(legend.position = "bottom") +
  scale_x_megabase() +
  labs(x = "Chr. coord. [Mb]",
       y = "",
       color = "Distance\nthreshold")

#- fig002, fig.width=10, fig.fullwidth=TRUE, fig.height=2, fig.cap="\\label{fig:pile}"

pile(map2(wgts, names(wgts), function(x, n) {
  .d <- as.data.frame(ranges(tracts(x)))
  .d$dist <- as.numeric(n)
  .d
})) %>%
  dplyr::arrange(desc(dist)) %>%
  dplyr::mutate(fdist = factor(dist)) %>%
  dplyr::filter(group_name == "ab93a2_t30") %>%
  {
    .d <- .
    ggplot(.d, aes(x = start, xend = end, y = fdist, yend = fdist, color = fdist)) +
      geom_segment(size = 3) +
      geom_point(data = .d[.d$SPI, ]) +
      scale_color_viridis_d(end = 0.9, begin = 0) +
      scale_alpha_continuous(trans = "log10", range = c(1, 0.6)) +
      guides(alpha = "none",
             color = "none") +
      theme_thesis() +
      scale_x_megabase() +
      labs(x = "Chr. coord. [Mb]",
           y = "Distance\nthreshold")
  }

#' Figure \ref{fig:width} represents the distribution of width
#' depending on the distance threshold chosen. As for figure
#' \ref{fig:pile}, the distance threshold does not influence the
#' resulting tracts much, except for really high values.

#- fig003, fig.height=7, fig.cap="\\label{fig:width}"

format_tenth_power <- function(x)
  parse(text = gsub(".*e+", "10^", (scales::scientific_format()(x))))

pile(map2(wgts, names(wgts), function(x, n) {
  .d <- as.data.frame(ranges(tracts(x)))
  .d$dist <- as.numeric(n)
  .d
})) %>%
  dplyr::arrange(desc(dist)) %>%
  dplyr::mutate(fdist = factor(dist)) %>%
  dplyr::filter(!SPI) %>%
  ggplot(aes(x = width, fill = fdist)) +
  geom_histogram(bins = 100) +
  facet_col1(dist~., scales = "free_y") +
  scale_x_log10(labels = format_tenth_power) +
  scale_fill_viridis_d(end = 0.9, begin = 0) +
  guides(fill = "none") +
  theme_thesis() +
  labs(x = "Tract Width [bp]",
       y = "")

#' # Influence over gBGC estimates
#'
#' Next I wanted to check that the way we discriminate conversion
#' tracts had no influence on the gBGC estimates we make.

wgts_counts <- parallel::mcMap(
  function(x, d) .counts(x),
  wgts, as.numeric(names(wgts)))


#- fig004, eval=FALSE
wgts_counts %>%
  map(function(d) {
  d %>%
    as.data.frame() %>%
    rownames_to_column("conv")
  }) %>%
  lfdf() %>%
  dplyr::rename(dist = id) %>%
  tibble::remove_rownames() %>%
  dplyr::select(dist, conv, donor, tract, spi, bord, rest) %>%
  data.table::fwrite(file = "../data/distances.tsv", sep = "\t")

#- fig005, fig.cap="\\label{fig:test}"

my_geom_text <- function(...) {
  geom_text(..., family = "Roboto Condensed")
}

wgts_counts %>%
  map(function(d) {
    f <- function(x) {
      x %>%
        clean_rest() %>%
        prop.tester() %>%
        decorate_signif()
    }
    try(f(d), TRUE)
  }) %>%
  flt_by(is.data.frame) %>%
  {
    .[sapply(., length) != 0]
  } %>%
  lfdf() %>%
  dplyr::mutate_at(vars(dev, ci_l, ci_r, p.val, df, id), as.numeric) %>%
  dplyr::filter(df > 10, names != "tract") %>%
  dplyr::rename(dist = id) %>%
  dplyr::mutate(plab = paste("p", ifelse(p.val < 1e-3, "< 0.001", paste("=", round(p.val, 3))))) %>%
  dplyr::mutate(names = factor(names,
                               levels = c("spi", "bord", "rest"),
                               labels = c("SMC", "MMC borders", "SMR"))) %>%
  ggplot(aes(x = factor(dist), y = dev)) +
  geom_errorbar(aes(ymin = ci_l, ymax = ci_r), position = position_dodge(width = 0.2),
                width = 0) +
  geom_point(aes(color = signif)) +
  my_geom_text(aes(label = paste("n =", df), y = ci_l), hjust = 1.1,
               color = colorblindr::palette_OkabeIto[1]) +
  my_geom_text(aes(label = plab, y = ci_r), hjust = -0.1,
            color = colorblindr::palette_OkabeIto[3]) +
  coord_flip() +
  scale_color_identity() +
  facet_col1(names ~ .) +
  theme_thesis() +
  labs(y = "Deviation from expected\nproportions of W>S",
       x = "Distance threshold [bp]") +
  theme(axis.line.y = element_blank(),
        panel.grid.major = element_line(linetype = "dotted", color = "gray"))

#' Figure \ref{fig:test} does not represents significance testing for
#' all converted markers taken jointly, because this proportion is not
#' affected by the distance threshold chosen. However, we can see that
#' single marker conversions are clearly GC-biased and that it does
#' not depend on the distance threshold chosen.
#'
#' Although the number of SMC decreases as the distance threshold
#' increases, probably because they are merged with a neighboring
#' conversion tract into a longer conversion tract.
#'
#' This could explain why MMC borders are significantly GC-biased for
#' the largest distance threshold.
#'
#' The case of isolated restorations is interesting because their
#' number is also a function of the distance threshold. For the lowest
#' distance threshold, we cannot count any restorations because they
#' lead to two markers being in two different tracts. For the largest
#' distance threshold, restorations tend to be not isolated, and the
#' probability that they are found in only one sample decreases,
#' because the proportion of converted genome per sample increases.
#' But we can clearly see that for reasonable distance threshold,
#' isolated restorations are clearly GC-biased.


#' ## Influence of distance threshold over number of events
#'
#' > - nombre de tracts de conversion
#' > - % du génome inclus dans des tracts (ça, ça ne devrait pas être trop sensible)
#' > - nombre de MMC (et % de MMC avec des marqueurs ‘restored')
#' > - nombre de SMC


prop_in_tract <- function(wgt) {
  mean(vapply(ranges(tracts(wgt)), function(x) {
    sum(width(x)) / seqlengths(wgt) * 100
  }, numeric(1)))
}

prop_mmc_with_restorations <- function(wgt) {
  mmcs <- cnp(ranges(tracts(wgt)))
  restorations <- keep_isolated_restored(wgt)
  mean(vapply(pmap(`%over%`, mmcs, restorations), function(x) {
    sum(x) / length(x)
  }, numeric(1)))
}

sstats <- pmap(function(wgt) {
  data.table(
    n_tracts = length(unlist(ranges(tracts(wgt)))),
    prop_in  = prop_in_tract(wgt),
    n_mmc    = length(unlist(cnp(ranges(tracts(wgt))))),
    n_smc    = length(unlist(spi(ranges(tracts(wgt))))),
    prop_mmc_with_restorations = prop_mmc_with_restorations(wgt)
  )
}, wgts)

sstats <- lfdf(sstats)
sstats$id <- as.integer(sstats$id)
sstats$prop_mmc_with_restorations <- sstats$prop_mmc_with_restorations * 100

h <- c("n_tracts" = "Total Tracts",
         "prop_in"  = "% Genome in tracts",
         "n_mmc"    = "MMCs",
         "n_smc"    = "SMCs",
         "prop_mmc_with_restorations" = "% MMCs with restorations")
local({
  d <- tidyr::gather(sstats, param, value, -id)
  p1 <- d %>%
    dplyr::filter(grepl("^n_", param)) %>%
    dplyr::mutate(param = h[param]) %>%
    ggplot(aes(x = id, y = value, color = param, group = param)) +
    geom_line(size = 0.3) +
    geom_point() +
    colorblindr::scale_color_OkabeIto() +
    scale_x_log10() +
    labs(x = "Distance threshold", y = "", color = "") +
    theme_light(base_family = "Roboto Condensed") +
    theme(legend.position = "top")
  p2 <- d %>%
    dplyr::filter(!grepl("^n_", param)) %>%
    dplyr::mutate(param = h[param]) %>%
    ggplot(aes(x = id, y = value, group = param)) +
    geom_line(size = 0.3) +
    geom_point() +
    scale_x_log10() +
    facet_wrap(~param, ncol = 2) +
    labs(x = "Distance threshold", y = "", color = "") +
    theme_light(base_family = "Roboto Condensed") +
    theme(legend.position = "top")
  p <- p1 / p2
  cairo_pdf("figures/distthres/figsum.pdf", width=5, height=5, family="Roboto Condensed")
  print(p)
  dev.off()
})

local({
  x <- copy(sstats)
  colnames(x) <- c(h, "distance threshold")
  x <- x[, c(6, 1:5)]
  print(xtable::xtable(x), include.rownames = FALSE)
})

## /* WIP



#' # Influence of distance threshold on distribution of D

wgts %>%
  map(function(x) {
    x %>%
      tracts() %>%
      ranges() %>%
      {
        shiftApply(1, . , . , distance)
      }
  })

wgts %>%
  map(
    function(dist) {
      dist %>%
        tracts() %>%
        ranges() %>%
        map(function(x) {
          data.table(dist = shiftApply(1, x, x, distance))
        }) %>%
        lfdf() %>%
        `colnames<-`(c("distance", "sample"))
    }) %>%
  lfdf() %>%
  `colnames<-`(c("distance", "sample", "thres")) %>%
  dplyr::mutate(thres = forcats::fct_rev(factor(as.integer(thres)))) %>%
  ggplot(aes(x = distance, fill = thres)) +
  geom_histogram(bins = 30) +
  facet_grid(thres ~ ., ) +
  scale_x_log10() +
  ## scale_y_log10() +
  scale_fill_viridiscrete() +
  guides(fill = "none") +
  theme_thesis()

## */

x <- wgts[["1000"]]

x.cnps <- unlist(cnp(ranges(tracts(x))))
snvs <- rowRanges(x)[isSNV(x)]

mean(countOverlaps(x.cnps, snvs))

## number of
length(CRanges(keepRestored(x)))
