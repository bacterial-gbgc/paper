# Produces the table figure that aims to represent general features of
# strains: divergence, gc content, $\Delta$GC content, number of
# tracts, ...

# load data
source("header.R")

# read reference recipient genomes
ref_spne <- readDNAStringSet("../data/AE007317.fasta")
ref_hpyl <- readDNAStringSet("/Users/samuelbarreto/these/bubendorfer/data-raw/receiver-26695.fasta")
ref_bsub <- readDNAStringSet("/Users/samuelbarreto/these/data/2018-11-19-nucmer-test/bs-168.fasta")
ref_abay <- DNAStringSet(ab$CR543861.1)
ref_strains <- list(ref_abay, ref_bsub, ref_hpyl, ref_spne)

# apply fun to each reference recipient genomes
apply_ref_strains <- function(fun) sapply(ref_strains, fun)

# correct aggregation for s.pneumoniae
do_on_strains <- function(fun, agg = sum) {
  .a <- lapply(list(.abay, .bsub, .hpyl), fun)
  .b <- lapply(list(.spne[[1]], .spne[[2]]), fun)
  .out <-  append(.a, match.fun(agg)(c(.b[[1]], .b[[2]])))
  setNames(.out, c("Ab", "Bs", "Hp", "Sp"))
}

# creates table
.dat <- tibble::tibble(
  # strains shortname
  strains = c("Ab", "Bs", "Hp", "Sp")
  # gc content
, gc = (apply_ref_strains(lf_cg)) * 100
  # genome size (Mb)
, l = (apply_ref_strains(width)) / 1e6
  # divergence (%)
, div = (unlist(do_on_strains(n_div, mean)) / (l * 1e6)) * 100
  # number of MMC and SMC
, mmc = unlist(do_on_strains(n_mmc, sum))
, smc = unlist(do_on_strains(n_smc, sum))
)


# better colnames
colnames(.dat) <- c("Species", "GC%", "Genome\nSize (Mb)", "Divergence (%)", "MMC", "SMC")

# round up significance digits
.dat$`Divergence (%)` <-    round(.dat$`Divergence (%)`, 2)
.dat$`Genome\nSize (Mb)` <- round(.dat$`Genome\nSize (Mb)`, 2)

# correct order to match dotplot
.dat <- .dat[c(1, 3, 4, 2), ]

# correct species name
.dat$Species <- c("A. baylyi", "H. pylori", "S. pneumoniae", "B. subtilis")

# print table to output file (options must be passed to print.xtable)
cat(print(xtable::xtable(.dat, align = c("r", ">{\\itshape}r", rep("l", 5))),
          booktabs = TRUE, include.rownames = FALSE),
    file = "../figures/fig4/tab.tex")

((ggplot(mtcars, aes(cyl, mpg)) +
   geom_point()) /
   (gridExtra::tableGrob(.dat, theme = gridExtra::ttheme_minimal()))
)
