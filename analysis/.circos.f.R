### .circos.f.R --- functions for generating circos plots
# Copyright (C) 2019  Samuel Barreto
# Author:  Samuel Barreto
# License: GPL3+ (see <https://www.gnu.org/licenses/gpl-3.0.txt>)

## source("header.R")
library(colorspace)

## ** Functions

## simple hash table creator
hasher <- function(keys, values) setNames(values, keys)

## write table with better defaults for circos
write_table <- function(x, outfile) {
  write.table(x, file = outfile, sep=" ", col.names = FALSE,
              row.names = FALSE, quote = FALSE)
}


## return an rgb suitable for circos
circos_rgb <- function(col) {
  paste0("(", concat(as.character(col2rgb(col))), ")")
}

## generate a level of viridis
.vir <- function(x) {
  .pal <- viridis::viridis(n = 100)
  .pal[x * 100L]
}


PAL <- colorblindr::palette_OkabeIto[c(2, 3)]
## entry point for coloring the plot
.encolor <- function(...) circos_rgb(...)

df_maker <- function(strain) {
  .n_samples <- length(tracts(strain))

  .tracts <- ranges(tracts(strain))
  .percent_ge <- sort(sapply(width(.tracts), sum) / seqlengths(strain))
  .dat <- as.data.frame(.tracts)

  .dat <- .dat[, c("group_name", "start", "end")]
  .dat$chr <- "chrI"

  .dat$pos <- hasher(names(.percent_ge), order(.percent_ge))[.dat$group_name]
  .dat <- .dat[order(.dat$pos), ]

  ..colors <- rep(c(PAL[1], PAL[2]), 50)
  .cols <- hasher(keys   = unique(.dat$group_name),
                  values = ..colors[1:.n_samples])

  .intvl <- round(seq(0.2, 0.9, length.out = .n_samples), 3)
  .r0 <- hasher(unique(.dat$group_name), .intvl)
  .r0 <- .r0[.dat$group_name]

  .stride <- .intvl[2] - .intvl[1]
  .r1 <- hasher(unique(.dat$group_name), .intvl + .stride)
  .r1 <- .r1[.dat$group_name]

  .fills <- .cols[.dat$group_name]


  .dat$fill <- .fills
  .dat$r0 <- .r0
  .dat$r1 <- .r1

  .dat
}

decorate_donut <- function(strain) {
  .dat <- df_maker(strain)
  .out <- with(.dat, cbind(chr, start, end, attr))
  rbind(.out,
        c("chrI", "0", seqlengths(strain),
          paste0("r0=", min(.dat$r0), "r,r1=", max(.dat$r1), "r,",
                 "fill_color=vvlgrey,stroke_color=vvlgrey")))
}

color_by_sample <- function(.data) {
  .names <- levels(.data$group_name)
  .cols <- hasher(.names, rep(c(PAL[1], PAL[2]), length(.names))[seq_along(.names)])
  .data %>% dplyr::mutate(col = .cols[group_name])
}

## * .circos.f.R ends here.
