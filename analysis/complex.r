#' ---
#' title: "Complex Tract"
#' author: "Samuel Barreto"
#' date: "`r Sys.Date()`"
#' output: tint::tintPdf
#' latexfonts:
#' - package: mathdesign
#'   options:
#'     - bitstream-charter
#' - package: nimbusmononarrow
#' ---

#+ knitr, echo=FALSE
opts_chunk$set(echo=FALSE, prompt=FALSE, comment=NA, message=FALSE,
               warning=FALSE, global.par = TRUE, fig.path = "figures/",
               fig.width=7, fig.height=5)

#+ functions,echo=FALSE

source("header.R")

#' # Restorations

#' In the A.baylyi dataset, I kept only sites that were within the
#' range of a conversion tract but not converted. These sites are
#' called restorations, i.e. sites that were included in the
#' heteroduplex but weren't converted to the donor haplotype, or were
#' restored back to the recipient's.
.rest <- keepRestored(.abay)

#' These sites are located like so on the recipient genome, with each
#' bar a 1/1000 of the genome length:

#+ figex, fig.cap="Distribution of restored markers on genome."
.hist(start(CRanges(.rest)), breaks = 1000)

#' So it seems there is a lot of redundancy between restored sites,
#' especially in the hotspot region. I think it has to do with the
#' fact that true “biological” heretoduplex in the hotspot region does
#' not include a list of donor/recipient markers, so they are detected
#' as restored while they were not even susceptible of being
#' converted.
#'
#' Figure \ref{fig:fig13} is the distribution of number of time a
#' given marker is detected as restored. Thus about 1200 markers are
#' detected only once, the rest is detected more frequently.

#+ fig13, fig.cap="\\label{fig:fig13}Distribution of occurence number of marker"
.plot(averell(countSelfOverlaps(CRanges(.rest))))

#+ fig14, fig.cap="\\label{fig:fig14}Distribution on genome of rarely restored markers."
.hist(start(keep_rare(CRanges(.rest))), breaks = 1000)

#' That means that to treat restored sites and take this redundancy
#' into account, I have to take only uniquely restored markers, i.e.
#' markers that are restored only in one independent lineage.
#'
#' Figure \ref{fig:fig14} is the same genome distribution when taking
#' into account only uniquely restored markers. We still see the a
#' sort of “hotspot” effect, albeit less so (check Y axis scale.)
#' Maybe the one about 2.75 Mb is a “biological” hotspot, i.e. a
#' restoration hotspot, a location that is frequently restored back to
#' its recipient haplotype when involved in a heteroduplex?
#'
#' If we count the occurences of W|S and S|W restorations in this
#' “filtered” dataset,

countWS(keep_rare(CRanges(.rest)))

#' We see no significant deviation from expected proportions. (Here,
#' when gBGC is true, we expect more S>W than W>S in the context of
#' restoration.)

.tidy(.abay_propt(countWS(keep_rare(CRanges(.rest)))))

#' However, most restoration events are not independant of their
#' flanking restorated markers; many markers are restored conjointly
#' with other. Figure \ref{fig:fig15} is the distribution of distance
#' to next or previous restored marker.

#+ fig15, fig.cap="\\label{fig:fig15}Distance between restored markers"
hist(
  log10(unname(
    do.call("c", lapply(.rest, function(x)
      c(.distance_to(precede, x, x),
        .distance_to(follow,  x, x)))))),
  breaks = 100,
  xlab = "Log10 distance to previous or next marker.",
  main = "")

#' So if only SMC are GC-biased in conversions, the corollary should
#' be that only SMR (single marker restorations) are GC biased as
#' well. So here I kept only isolated restoration, i.e. restorations
#' that are further than 1e3 bp away from others (given the
#' distribution of distance above).

.rest_isol <- (flt_by(.rest, is_isolated))

#' It seems that SMR are also GC-biased:

countWS(CRanges(.rest_isol))

#' And significantly so:

.tidy(.abay_propt(countWS(CRanges(.rest_isol))))

## only rarely restored markers, i.e. restored in only one lineage.
.rare_rest <- keep_rare(CRanges(.rest))
## basically a hack to split according to unlisted GRanges rownames
## convention to separate by a “.” the old list elements.
.rr_lst <- split(
  as(.rare_rest, "GRanges"),
  sapply(strsplit(names(.rare_rest), "\\."), `[`, 1))

## count number of overlaps between a given tract and a restored
## marker: count the number of restored marker per tracts.
.b1 <- delist(map2(.rr_lst, ranges(tracts(.abay)), function(x, y) {
  countOverlaps(y, x)
}))

#' Of 2000 tracts, about 13% contain at least one restored marker:
william(.b1 > 0)

#' Figure \ref{fig:fig6} is the distribution of number of restored
#' markers per tract, only for tracts that contain a restored marker.
#' The great majority of tracts with restorations (about 150) have
#' only one restoration in it. Maximum is for the tract with 78
#' restorations.

#+ fig6, fig.cap="\\label{fig:fig6}Count the number of tract with a given number of restoration"
plot(averell(.b1[.b1 > 0]),
     main = "", xlab = "# of restorations", ylab = "# of tracts")

#' I expected a correlation between tract length and number of
#' restoration, but it turned out not to be the case. Thus, number of
#' restorations per tract does not depend on its length, but rather on
#' a more complex mechanism (see \ref{fig:fig5}). Maybe it depends
#' also on the local divergence?

#+ fig5, fig.cap="\\label{fig:fig5}Correlation between tract length and number of restorations"
## dataframe with two columns ():
.b2 <- pile(map2(.rr_lst, ranges(tracts(.abay)), function(x, y) {
  data.frame(
    length = width(y),                  # 1) tract length
    n_rstr = countOverlaps(y, x)        # 2) number of restorations
  )
}))

ggplot(.b2[.b2$length > 1 & .b2$n_rstr > 0, ], aes(x = length, y = n_rstr)) +
  geom_point() +
  no_x() + lb() +
  labs(y = "# Restoration", x = "Tract length")

#+ chunk10, cache=TRUE
## a bit involved code, that returns for each tract in each lineage
## the number of template switch between donor and recipient
## haplotype. FIXME: maybe could benefit a little speed up by querying
## only those tracts where we know there are restorations.

## the algorithm idea is to iterate over each ranges, to get 1) the
## restoration in this ranges and 2) the conservations (via
## subsetByOverlaps) and combine them together as a GRanges, with a
## column “rest” for restored that is TRUE or FALSE if a marker is
## restored. Then this column is runLength encoded (fast operation),
## and the number of runValues (TRUE or FALSE) is stored in a
## pre-alocated vector. Number of template switch is equal to (x - 1)
## / 2.
.c5 <- Map(function(x, y, z) {
  .lengths <- vector("integer", length(y))
  for (t in seq_along(y)) {
    .c1 <- y[t, ]
    .c2 <- subsetByOverlaps(z, .c1)
    .c3 <- subsetByOverlaps(x, .c1)
    if (length(.c3) < 1) {
      .lengths[t] <- 0
      next
    } else {
      .c3 <- as(.c3, "GRanges")
      .c2 <- as(.c2, "GRanges")
      .c3$rest <- FALSE
      .c2$rest <- TRUE
      .lengths[t] <-
        length(runValue(Rle(sort(c(.c2[, "rest"], .c3[, "rest"]))$rest)))
    }
  }
  .lengths
}, .rr_lst, ranges(tracts(.abay)), tracts(.abay))

#' For those tracts that contain restorations, how many time do we see
#' a template switch? How many time do we switch from recipient to
#' donor haplotype? Figure \ref{fig:fig8} represent the number of
#' tracts with a given amount of template switch. It follows roughly
#' the sequence of odd numbers, starting from 3. 3 template switch
#' corresponds to only one switch to the recipient haplotype (i.e. one
#' restoration event), 5 to two, 7 to three ... $2n + 1$ to $n$. I
#' supposed that the number of template switch depended on the tract
#' length, but it turned out not to be the case (see figure
#' \ref{fig:fig9}).

#+ fig8, fig.cap="\\label{fig:fig8}Number of template switch"
.c6 <- delist(.c5)
plot(averell(.c6[.c6 > 0]), xlab = "# of template switch", ylab = "# of tracts")


#+ fig9, fig.cap="\\label{fig:fig9}Correlation between tract lengths and template switch"
.c7 <- pile(map2(.c5, ranges(tracts(.abay)), function(x, y) {
  data.frame(l = width(y), swtch = x)
}))

ggplot(.c7[.c7$swtch > 0, ], aes(x = l, y = swtch)) +
  geom_point(alpha = 0.2) +
  scale_y_continuous(breaks = seq(3, 23, 4)) +
  no_x() + lb() +
  labs(x = "Tract Lenth", y = "# of template switch")
