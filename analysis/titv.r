#' ---
#' title: "Transition bias"
#' date: "`r Sys.Date()`"
#' author: "Samuel Barreto"
#' output: tint::tintPdf
#' latexfonts:
#' - package: libertinus
#'   options:
#'     - osf
#'     - p
#' - package: gillius2
#' - package: zi4
#'   options:
#'     - varqu
#'     - varl
#' - package: libertinust1math
#' ---

#- r knitr_init, echo=FALSE, cache=FALSE
knitr::opts_chunk$set(
  echo=FALSE,
  prompt=FALSE,
  comment=NA,
  message=FALSE,
  cache=TRUE,
  warning=FALSE,
  global.par=TRUE,
  tidy=FALSE,
  fig.width=7,
  fig.height=5,
  dev="pdf",
  fig.showtext=TRUE,
  fig.path="figures/titv/",
  cache.path=".cache/titv/")

#' This reports analyze the transition and transversion bias of conversions.
library(kableExtra)
library(data.table)

source("header.R")

.. <- list()

# nice latex table formatting
..$kable <- function(...) {
  knitr::kable(..., align = "c", booktabs = TRUE, format = "latex")
}

# contingency table creation
..$ctmat <- function(a, b) t(matrix(c(a, b), ncol = 2))

# proportion test with clear columns
..$prop.test <- function(.matrix, ..., .colnames = c("e1", "e2")) {
  .tab <- broom::tidy(prop.test(.matrix), ...)
  .tab <- .tab[, 1:7]
  colnames(.tab) <- c(.colnames, "stat", "p.value", "df", "[low", "high]")
  .tab
}

# nice proportion test table
..$kable.prop.test <- function(a, b, ..., .colnames = c("a", "b")) {
  ..$kable(..$prop.test(..$ctmat(a, b), .colnames = .colnames), ...)
}

#' Table \ref{tab:chi2} compares the proportions of conversions that
#' are transversions to the proportion of markers that are
#' transversions. There is a significant excess of transitions in
#' conversions, although it is hard to believe that it biologically
#' relevant. Single marker conversions and restorations are
#' preferentially transversions.
#'
#' This is another evidence that it is probably the same mechanism
#' that generates single marker restorations and single marker
#' conversions.

..$data <-
  lapply(list(cnp, function(x) border(cnp(x)), spi),
         function(f) total(f(tracts(.abay)), countTsTv))
..$data[[4]] <-
  countTsTv(as(unlist(keep_isolated_restored(.abay)),
               "ConversionRanges"))

..$data <- as.data.table(pile(..$data))
..$data$name <- c("smc/mmc", "mmc borders", "smc", "restored")
..$data <- ..$data[, .(name, transversion, transition)]
..$donor_tstv <- countTsTv(donorSites(.abay))

#' \newenvironment{myfont}{\sffamily\scriptsize\centering}{}
#- fig000, results="asis"

..$texble <- function(.tab, caption) {
  .dat <- xtable::xtable(.tab, caption = caption)
  function(...) {
    print(.dat, include.rownames = FALSE, latex.environment = "myfont", ...)
  }
}

local({
  .dat <- pile(map2(
    ..$data$transversion, ..$data$transition,
    function(x, y) {
      ..$ctmat(a = c(x, y), b = ..$donor_tstv) %>%
        prop.test() %>%
        broom::tidy()
    }))
  .dat <- .dat[c(1, 2, 4, 6, 7)]
  .dat$name <- ..$data$name
  .dat <- .dat[c(6, 1:5)]
  .dat <- cbind(.dat, ..$data[, 2:3])
  colnames(.dat) <- c(
    "name", "tv conv", "tv markers", "p.value", "[low", "high]",
    "tv", "ti")
  ..$texble(.dat, caption = "\\label{tab:chi2}\
Comparison to marker proportions of transversions for all markers, \
MMC borders, SMCs and restored markers.")()
})


addRefAlt <- function(x) {
  .snv <- transformr:::.keep_snv(x)
  .ref <- as.character(ref(.snv))
  .alt <- as.character(alt(S4Vectors::expand(.snv)))
  .snv$refalt <- paste(.ref, .alt, sep = ">")
  as.data.frame(mcols(.snv))
}

.x <- as.data.table(lfdf(Filter(function(x) nrow(x) > 0, map(spi(tracts(.abay)), addRefAlt))))
.x[, ws := transformr:::.wsTable()[refalt]]
.x[, ti := transformr:::.tstvTable()[refalt]]

add_refalt_donor_sites <- function(.donor_sites) {
  .ds <- rowRanges(.donor_sites)
  .ds$tn <- NA_integer_
  .ds_cr <- as(.ds, "ConversionRanges")
  .ds_cr_refalt <- addRefAlt(.ds_cr)
  as.data.table(.ds_cr_refalt)
}

.y <- add_refalt_donor_sites(donorSites(.abay))
.y[, ws := transformr:::.wsTable()[refalt]]
.y[, ti := transformr:::.tstvTable()[refalt]]

.x1 <- .x[, .N, by = .(ws, ti)][ws %in% c("W>S", "S>W")]
.y1 <- .y[, .N, by = .(ws, ti)][ws %in% c("W>S", "S>W")]

.y[, .N, by = ti][, prop.table(N)]

.ir <- keep_isolated_restored(.abay)
.ir <- .ir[sapply(.ir, length) > 0, ]

.ir_cnts <- map(.ir, function(x) {
  x$tn <- NA_integer_
  as(x, "ConversionRanges")
}) %>%
  map(function(x) {
    .snv <- transformr:::.keep_snv(x)
    .ref <- as.character(ref(.snv))
    .alt <- as.character(alt(S4Vectors::expand(.snv)))
    .snv$refalt <- paste(.alt, .ref, sep = ">")
    as.data.table(mcols(.snv))[, .(refalt)]
  })

.ir_cnts <- lfdf(.ir_cnts)

.ir_cnts[, c("ws", "ti") := list(transformr:::.wsTable()[refalt],
                                 transformr:::.tstvTable()[refalt])]

.t_ir <- .ir_cnts[, .N, by = .(ws, ti)][ws %in% c("W>S", "S>W")]
.t_ir <- dcast(.t_ir, ti ~ ws, value.var = "N")
.t_x1  <- dcast(.x1, ti ~ ws, value.var = "N")
.t_y1 <- dcast(.y1, ti ~ ws, value.var = "N")
.t_ir$type <- "restoration"
.t_x1$type <- "SMCs"
.t_y1$type <- "markers"

.t <- rbind(.t_y1, .t_x1, .t_ir)[, c(4, 1, 3, 2)]
setnames(.t, "ti", "ts/tv")
.t$vs_marker <- NA
.t$vs_tv <- NA
.t$vs_marker[3] <- prop.test(as.matrix(.t[`ts/tv` == "transition"][c(1, 2), 3:4]))$p.value
.t$vs_marker[5] <- prop.test(as.matrix(.t[`ts/tv` == "transition"][c(1, 3), 3:4]))$p.value
.t$vs_marker[4] <- prop.test(as.matrix(.t[`ts/tv` == "transversion"][c(1, 2), 3:4]))$p.value
.t$vs_marker[6] <- prop.test(as.matrix(.t[`ts/tv` == "transversion"][c(1, 3), 3:4]))$p.value
.t$vs_tv[3] <- prop.test(as.matrix(.t[`type` == "SMCs"][, 3:4]))$p.value
.t$vs_tv[5] <- prop.test(as.matrix(.t[`type` == "restoration"][, 3:4]))$p.value
.t[, prop := (.t[, 3] / (.t[, 3] + .t[, 4])) * 100]

.t <- .t[, c(1, 2, 3, 4, 7, 5, 6)]
options("digits" = 4)
print(xtable::xtable(.t, digits=3), include.rownames = FALSE,
      booktabs = TRUE)

.p1 <- local({
  .dat <- pile(map2(
    ..$data$transversion, ..$data$transition,
    function(x, y) {
      ..$ctmat(a = c(x, y), b = ..$donor_tstv) %>%
        prop.test() %>%
        broom::tidy()
    }))
  .dat <- .dat[c(1, 2, 4, 6, 7)]
  .dat$name <- ..$data$name
  .dat <- .dat[c(6, 1:5)]
  .dat <- cbind(.dat, ..$data[, 2:3])
  colnames(.dat) <- c(
    "name", "tv_conv", "tv_markers", "p.value", "low", "high",
    "tv", "ti")
  .dat %>%
    dplyr::transmute(
      name = name,
      dev = tv_conv - tv_markers,
      low = low,
      high = high,
      n = ti + tv,
      signif = ifelse(p.value < 0.05, "black", "gray"),
      name = factor(name,
                    levels = rev(c("smc/mmc", "smc", "mmc borders", "restored")),
                    labels = rev(c("All Converted Markers",
                                   "Single Marker Conversions",
                                   "Multiple Markers\nConversions Borders",
                                   "Restored Markers")))) %>%
    ggplot(aes(x = name, y = dev, ymin = low, ymax = high, color = signif)) +
    geom_hline(yintercept = 0, color = "#482677") +
    geom_linerange() +
    geom_text(
      aes(label = n, y = 0),
      hjust = 1.1, vjust = -1,
      family = "sans",
      size = 3,
      color = gray(0.2)) +
    geom_point() +
    coord_flip() +
    scale_color_identity() +
    scale_y_continuous(
      breaks = seq(0, 0.2, 0.1),
      limits = c(-0.1, 0.48)) +
    theme_samuer(base_family = "sans", plot_margin = margin(0, 0, 5, 0)) +
    theme(
      strip.text = element_text(size = 10, face = "bold"),
      panel.grid.minor.x = element_blank(),
      panel.spacing.y = unit(0.2, "lines")) +
    guides(shape = "none") +
    no_x() + lb() +
    labs(x = "",
         y = "Deviation from donor/recipient\nproportions of transversions")
})

decorate_plot_with_images <- function(x, ...) {
  .di <- function(fig, y) draw_image(fig, -0.2, y, scale = 0.25)
  .outplot <- ggdraw(x) +
    .di("../figures/1_all.png", 0.3) +
    .di("../figures/3_spi.png", 0.12) +
    .di("../figures/4_cnpextr.png", -0.1) +
    .di("../figures/8_rest.png", -0.25)
  ggsave(plot = .outplot, ...,
         width = 4.086667, height = 2.210000)
}

decorate_plot_with_images(.p1, "../figures/titv.pdf")

#' However type II error risk is quite high, as shown by table \ref{tab:pwr}

#- fig001, results="asis"

..$texble(
  dplyr::mutate(
    ..$data,
    prop = map2_dbl(transversion, transition, function(x, y) prop.table(c(x, y))[1]),
    power = Map(function(h, n1, n2) {
      h = h - prop.table(..$donor_tstv)[1]
      pwr::pwr.2p2n.test(h, n1, n2)$power
    }, prop, transversion, transition)),
  caption = "\\label{tab:pwr}Power associated with sample size")()
