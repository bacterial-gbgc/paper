#!/usr/bin/env perl

use strict;
use warnings;
use v5.24;

use FindBin qw($Script);
use File::Basename;
use File::Copy;
use Cwd qw(abs_path);
use POSIX qw(strftime);

my $EXE = basename($Script);

# ------------------------------------------------------------------------

my ($prefix) = shift or die "Need prefix";


my $n = 0;

while (<>) {
    chomp;
    next if m/%/;
    if (m/includegraphics/) {
        $n += 1;
        my $in = parse_file_name($_);
        my $ff = parse_file_format($in);
        my $out = "${prefix}_$n$ff";
        copy($in, $out) or die "Copy failed: $!";
    }
}

# -------------------------------------------------------------------------
sub parse_file_format {
    my ($fn) = @_;
    my @exts = qw(.pdf .png .jpg);
    my ($name, $dir, $ext) = fileparse($fn, @exts);
    return $ext;
}


sub parse_file_name {
    my ($line) = @_;
    $line =~ /{(.*)}/;
    return $1;
}

sub msg {
    my $time = strftime "%H:%M:%S", localtime;
    my $msg = "# [$EXE] ($time): @_\n";
    print STDERR $msg;
}

sub err { msg(@_) && exit(1); }

sub dbg { say STDERR @_ if $ENV{'DEBUG'}; }

sub get_cmd {
    my ($cmd) = @_;
    ( my $msg = $cmd) =~ s/[\n\r\\]//g;
     dbg("Running: '$msg'");
     return `$cmd`;
}

sub run_cmd {
    my ($cmd) = @_;
    dbg("Running: $cmd");
    system("bash", "-c", $cmd) == 0 or err("Error $? running command: $!");
}

__END__
