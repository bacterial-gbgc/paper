.PHONY: help all view

pdflatex = pdflatex -interaction=batchmode $(1)
date := $(shell date +%Y%m%d)

help:
	$(info make view -- preview article pdf and watch for updates)
	$(info make all  -- compile the document)

all: \
	article-plos.pdf \
	supmat.1.pdf \
	figures \
	figures.zip \
	article.pdf \
	plos-submission.tex

.PHONY: docx
docx: article_$(date).docx supmat.1_$(date).docx figures.zip
	zip article_$(date) $^ article.pdf supmat.1.pdf figures.zip

figures.zip:
	zip figures fig_* fig_sup_*

.PHONY: figures
figures:
	cat article.tex  | scripts/make_fig.pl "fig"
	cat supmat.1.tex | scripts/make_fig.pl "fig_sup"

.PHONY: clean
clean:
	rm -f plos-submission.tex article-plos.*

plos-submission.tex: article-plos.tex article.bbl figures/fig4/tabular.tex .header.tex
	sed -e '/%BIBLIOGRAPHY%/r $(word 2,$^)' \
		-e '/%FIG4TAB%/r $(word 3,$^)' \
		-e '/%HEADER%/r $(word 4,$^)' \
		$< \
	| egrep -v '\input' > $@


# une fois le pdf compilé, une version du document tex est compilée
# contenant les références incluses.
article-plos.pdf: article-plos.tex

# article-plos.tex est le fichier à partir duquel la version pour plos
# est compilée.
# c'est la même que le preprint mais sans les figures dans le texte.
article-plos.tex: article.tex article.pdf
	egrep -v '\includegraphics' $< > $@

%.bbl: %.tex
	bibtex $(basename $<)

%.pdf: %.tex
	latexmk -f -quiet $< 2> /dev/null > /dev/null

%_$(date).docx: %.tex
	pandoc -i $< -o $@

view:
	latexmk -f -pvc -quiet article.tex
